OS-Project1
===========

User Level Thread Library

This thread library provides basic interfaces like init, join, joinAll, exit, wait and signal. This program is just 
an illustration of a thread library and does not provides any guarantees. It should be used for educational purposes only.
A brief information about the entire thread module including how to use the interface in the threaded program will be 
uploaded shortty.

Good Luck!!
