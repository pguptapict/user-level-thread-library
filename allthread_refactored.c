/* 
*  User Level Non-preemptive Thread Library 
*  This library provides thread operations to the program.
*
*/


#include<ucontext.h>
#include<unistd.h>
#include<sys/types.h>
#include<stdlib.h>
#include "mythread.h"
#include<stdio.h>

const BUFFERSIZE = 8448;


/*********************** Structs definitions *******************/

typedef struct thread thread;
typedef struct ready_q ready_q;
typedef struct wait_q wait_q;
typedef struct join_q join_q;
typedef struct semaphore_q semaphore_q;
volatile static int id_counter = 0;
ucontext_t main_context;



struct thread{
	ucontext_t cntx;
	int thread_id;
	thread *parent;
	int no_of_alive_childs;
	int joined_child;
	int blocked;
	int yield_flag;
	int wait_flag;
	int isalive;
	thread *next;	
};

thread *thread_head = NULL;
thread *thread_tail = NULL;


struct ready_q{
	thread *thread;
	ready_q *next;
};

ready_q *ready_head=NULL;
ready_q *ready_tail=NULL;


struct join_q{
	thread *thread;
	join_q *next;
};

join_q *join_head = NULL;
join_q *join_tail = NULL;


struct semaphore_q{
	int initialvalue;
	semaphore_q *next;
};

semaphore_q *sem_head = NULL;
semaphore_q *sem_tail = NULL;

struct wait_q{
	thread *thread;
	semaphore_q *sem_blocked_on;
	wait_q *next;
};

wait_q *wait_head = NULL;
wait_q *wait_tail = NULL;


/********************** Structure Definition Ends ***********************/



/********************** Function Declerations ***********************/
void MyThreadInit(void(*funct)(void *), void *args);
MyThread MyThreadCreate(void(*funct)(void *), void *args);
void appendToReadyQueue(thread *thread);
void appendToThreadsList(thread *thread);
void MyThreadYield(void);
void MyThreadExit(void);
int MyThreadJoin(MyThread node);
void appendToJoinList(thread *thread);
void removeFromJoinList(join_q *node);
MySemaphore MySemaphoreInit(int initialValue);
void MySemaphoreWait(MySemaphore sem);
void appendToWaitList(thread *thread, semaphore_q *sem_blocked_on);
void MySemaphoreSignal(MySemaphore sem);
void removeFromWaitList(MySemaphore semaphore);
int MySemaphoreDestroy(MySemaphore sem);
void freeMemory(ready_q *y);

/********************** Function Decleration Ends ***********************/


/*  This function initializes the main thread. It is invoked only by the Unix Process.
*   
*   Input:
*   funct : This is the function in which the new thread start executing.
*   args  : Arguments passed to funct function. 
*
*   Returns:
*   Nothing(void)	  
*/

void MyThreadInit(void(*funct)(void *), void *args){

	MyThread *thread;
	getcontext(&main_context);
		
	if(ready_head==NULL){
		thread = MyThreadCreate(funct,args);
		swapcontext(&main_context,&ready_head->thread->cntx);	
	}
}


/*
*  MyThreadCreate() creates a new thread and appends it in the ready queue.
*   
*   Input:
*   funct : This is the function in which the new thread start executing.
*   args  : Arguments passed to funct function. 
*
*   Returns:
*   The reference to the newly created thread.	  
*/

MyThread MyThreadCreate(void(*funct)(void *), void *args){
	ucontext_t cntx;
	thread *thread;
	thread = (struct thread *)malloc(sizeof(thread));
	if(thread == NULL)
		printf("\n Error in allocating memory");
	
	thread->thread_id = id_counter++;
	if(ready_head == NULL)
		thread->parent = NULL;
	else		
		thread->parent = ready_head->thread;
	thread->no_of_alive_childs = 0;
	thread->joined_child = 0;
	thread->blocked = 0;
	thread->yield_flag=0;
	thread->wait_flag=0;
	thread->isalive = 1;
	thread->next = NULL;

	getcontext(&cntx);
	cntx.uc_stack.ss_sp = malloc(BUFFERSIZE * sizeof(char));
	if(cntx.uc_stack.ss_sp == NULL)
		printf("\n Error in allocating memory");
	cntx.uc_stack.ss_size = BUFFERSIZE;
	cntx.uc_link = &main_context;	
	
	makecontext(&cntx,(void(*)(void))funct,1,args);
	thread->cntx = cntx;
	if(ready_head != NULL)
		ready_head->thread->no_of_alive_childs++;
	appendToThreadsList(thread);
	appendToReadyQueue(thread);
	return (MyThread)thread;
}



/*
*  appendToReadyQueue() appends the newly created thread to the ready Queue.
*   
*   Input:
*   thread * : The reference to the newly created thread.
*
*   Returns:
*   Nothing(void).	  
*/

void appendToReadyQueue(thread *thread){
	ready_q *node = (ready_q *)malloc(sizeof(ready_q));
	if(node == NULL)
		printf("\n Error in allocating memory");
	node->thread = thread;
	node->next = NULL;
	if(ready_head==NULL){	
		ready_head = node;
		ready_tail = node;
	}else{
		ready_tail->next = node;
		ready_tail = node;
	}
}



/*
*  appendToThreadList() appends the newly created in the threads list.
*   
*   Input:
*   thread * : The reference to the newly created thread.
*
*   Returns:
*   Nothing(void)	  
*/

void appendToThreadsList(thread *thread){
	if(thread_head==NULL){	
		thread_head = thread;
		thread_tail = thread;
	}else{
		thread_tail->next = thread;
		thread_tail = thread;
	}
}



/*
*  MyThreadYield() yields the currently running thread and sets the next thread in the ready queue
*  to execution.
*   
*   Input:
*   void.
*
*   Returns:
*   Retuns to the next thread in the ready queue.	  
*/

void MyThreadYield(void){
	ucontext_t cntx;
	
	if(ready_head!=NULL && ready_head->next!=NULL){
		getcontext(&cntx);
		if(ready_head->thread->yield_flag != 1){
			ready_head->thread->cntx = cntx;
			ready_head->thread->yield_flag = 1;
			appendToReadyQueue(ready_head->thread);
			ready_head = ready_head->next;
			swapcontext(&cntx,&ready_head->thread->cntx);
		}
	
	if(ready_head!=NULL)
		ready_head->thread->yield_flag = 0;
	}
		
}


/*
*  MyThreadExit() exits the currently executing thread.
*   
*   Input:
*   void. 
*
*   Returns:
*   Return to the next thread in the ready queue.	  
*/

void MyThreadExit(void){
	join_q *q = join_head;
	ucontext_t cntx;
	ready_q *node_to_be_deleted = ready_head;
	
	while(q!=NULL){
		if(q->thread == ready_head->thread->parent){
			if(ready_head->thread->joined_child == 1){
				q->thread->no_of_alive_childs--;
				ready_head->thread->joined_child = 0;
				appendToReadyQueue(q->thread);
				removeFromJoinList(q);
			}else{	
				q->thread->no_of_alive_childs--;
				if(q->thread->no_of_alive_childs <= 0){
					appendToReadyQueue(q->thread);
					removeFromJoinList(q);
				}
			}
			break;
		}
		q = q->next;
	}
	if(ready_head!=NULL && ready_head->next!=NULL)	{
		ready_head->thread->isalive = 0;
		ready_head = ready_head->next;
		swapcontext(&node_to_be_deleted->thread->cntx,&ready_head->thread->cntx);
	}else{
		swapcontext(&node_to_be_deleted->thread->cntx,&main_context);	
	}

}


void freeMemory(ready_q *y){
	//Free memory allcocated to thread
	thread *q = thread_head;
	thread *r = q->next;
	while(q!=NULL){
		if(q == y->thread){
			thread_head = thread_head->next;
			free(q);
			break;
		}
		else if(r == y->thread) {
			
				q->next = r->next;
				break;
			
		}
		r = r->next;
		q = q->next;
	
	}
	y->thread = NULL;
	y->next = NULL;
}


/*
*  MyThreadJoin() joins the current thread with the thread passed in as the parameter.
*   
*   Input:
*   MyThread node : Reference to the thread to which the currently executing thread will join.
*
*   Returns:
*   0 : On Success
*  -1 : On Failure. This happens if specified thread is not an immediate child of invoking thread.  
*/

int MyThreadJoin(MyThread node){
	ucontext_t cntx;
	int isAlive = 0,isImmidiate=0;
	thread *t = thread_head;
	// Check if the child is the immidiate child
	while(t!=NULL){
		if(t->thread_id == ((thread *)node)->thread_id && t->isalive == 1)
		{	
			isAlive = 1;
			if(((thread *)node)->parent == ready_head->thread)
				isImmidiate = 1;
			break;	
		}
		t = t->next;
	}
	if(isImmidiate == 0)
		return -1;
	
	if(isAlive){
		ready_q *q = ready_head;
		ready_head = ready_head->next;
		getcontext(&cntx);
		if(ready_head->thread->blocked != 1){
			q->thread->cntx = cntx;
			((thread *)node)->joined_child = 1;			
			appendToJoinList(q->thread);
			swapcontext(&cntx,&ready_head->thread->cntx);
		}
		if(ready_head!=NULL)
			ready_head->thread->blocked = 0;
		return 0;
	}else{ return 1;
	}
}


/*
*  MyThreadJoinAll() blocks the current thread till all its immidiate childs exits.
*   
*   Input:
*   void.
*
*   Returns:
*   If all child threads are dead, the function returns immidiately. 
*   If child threads exists, return to the next thread in the ready queue.  
*/

void MyThreadJoinAll(void){
	ucontext_t cntx;
	if(ready_head!=NULL && ready_head->next!=NULL){
		
		ready_q *q = ready_head;
		if(q->thread->no_of_alive_childs != 0){
			ready_head = ready_head->next;
			getcontext(&cntx);
			if(ready_head->thread->blocked != 1){
				q->thread->cntx = cntx;
				appendToJoinList(q->thread);
				swapcontext(&cntx,&ready_head->thread->cntx);
			}
		}
		if(ready_head!=NULL)	
			ready_head->thread->blocked = 0;
	}
}



/*
*  appendToJoinList(thread *) : Appends to the thread to the join list of all the threads .
*   
*   Input:
*   thread * : Reference to the thread that is to be inserted in the join List.
*
*   Returns:
*   void.
*   
*/

void appendToJoinList(thread *thread){
	 
	join_q *node = (join_q *)malloc(sizeof(join_q));
	if(node == NULL)
		printf("\n Error in Allocating memory\n");
	node->thread = thread;
	node->thread->blocked = 1;
	node->next=NULL;
	if(join_head==NULL){	
		join_head = node;
		join_tail = node;
	}else{
		join_tail->next = node;
		join_tail = node;
	}
}


/*
*  removeFromJoinList(join_q *) : This is called when a child exists causing a parent to return back to ready queue.
*   
*   Input:
*   join_q * : Reference to the node that is to be removed from the list.
*
*   Returns:
*   void.
*   
*/

void removeFromJoinList(join_q *node){
	join_q *q = join_head;
	join_q *r = join_head->next;
	if(q == node)
		join_head = join_head->next;

	else if(join_tail == node){
		while(q->next!=node)
			q = q->next;
		q->next = NULL;
		join_tail = q;
	}
	else {
		while(r!=NULL){
			
			if(r == node){

				q->next = r->next;
				r->next = NULL;
				break;
			}
			r = r->next;
			q = q->next;
		}
	}
		
}

	




//--------------------------------------------------SEMAPHORE ROUTINES --------------------------------------------------//



/*
*  MySemaphoreInit(int) : Initializes a new semaphore with the initial value passes as the parameter.
*   
*   Input:
*   int initialvalue : The value with which to initialize the semaphore.
*
*   Returns:
*   MySemaphore : Returns the reference of the newly created semaphore.
*   
*/

MySemaphore MySemaphoreInit(int initialValue){
	semaphore_q *node = (semaphore_q *)malloc(sizeof(semaphore_q));
	if(node == NULL)
		printf("\n Error allocating memory\n");

	node->initialvalue = initialValue;
	node->next = NULL;
	if(sem_head == NULL){
		sem_head = node;
		sem_tail = node;
	}else{
		sem_tail->next = node;
		sem_tail = node;
	}
	return (MySemaphore)node;
}



/*
*  MySemaphoreWait(MySemaphore sem) : Performs a wait operation on the Semaphore sem.
*   
*   Input:
*   MySemaphore sem : Reference of the semaphore on which the wait is to be performed.
*
*   Returns:
*   Return back to the calling thread initialvalue is greater than 0.
*   Block the thread and return to the next thread in the ready queue if initialvalue is less than or equal to 0.
*/

void MySemaphoreWait(MySemaphore sem){

	semaphore_q *q = sem_head;
	ucontext_t cntx;
	while(q!=NULL){
		if(sem == q){
			 if(q->initialvalue > 0){
				q->initialvalue--;
				
			}
			else{
				getcontext(&cntx);
				if(ready_head->thread->wait_flag !=1 && ready_head!=NULL){
					ready_q *n = ready_head;
					ready_head = ready_head->next;	
					n->thread->cntx = cntx;
					appendToWaitList(n->thread, (semaphore_q *)sem);
					
					if(ready_head!=NULL)
						swapcontext(&cntx,&ready_head->thread->cntx);
					else
						swapcontext(&cntx,&main_context);						
				}
				if(ready_head!=NULL)				
					ready_head->thread->wait_flag = 0;
			}
			break;
		}
		q = q->next;
	}
}	



/*
*  appendToWaitList(thread *thread, semaphore_q *sem_blocked_on) : Initializes a new semaphore with the initial 
*                                                                  value passes as the parameter.
*   
*   Input:
*   thread *thread : Reference of the thread to add in the list.
*   semaphore_q *  : Reference of the semaphore for which the thread is waiting.
*
*   Returns:
*   void.
*   
*/

void appendToWaitList(thread *thread, semaphore_q *sem_blocked_on){
	wait_q *node = (wait_q *)malloc(sizeof(wait_q));
	if(node==NULL)
		printf("\n Error in allocating memory");
	node->thread = thread;
	node->sem_blocked_on = sem_blocked_on;
	node->next = NULL;
	if(wait_head == NULL){
		wait_head = node;
		wait_tail = node;
	}else{
		wait_tail->next = node;
		wait_tail = node;
	}
}



/*
*  appendToWaitList(thread *thread, semaphore_q *sem_blocked_on) : Initializes a new semaphore with the initial 
*                                                                  value passes as the parameter.
*   
*   Input:
*   thread *thread : Reference of the thread to add in the list.
*   semaphore_q *  : Reference of the semaphore for which the thread is waiting.
*
*   Returns:
*   void.
*   
*/

void MySemaphoreSignal(MySemaphore sem){
	semaphore_q *q = sem_head;
	while(q!=NULL){
		if(sem == q){
			q->initialvalue++;
			if(q->initialvalue>0){
				wait_q *n = wait_head;
				while(n!=NULL){
					if(n->sem_blocked_on == sem){
						n->thread->wait_flag = 1;
						appendToReadyQueue(n->thread);
						removeFromWaitList(sem);
						break;
					}
					n = n->next;
				}
			}
			break;
		}
	q = q->next;
	}
}



/*
*   removeFromWaitList(MySemaphore sem) : Removes the thread that is waiting for semaphore sem.
*   
*   Input:
*   MySemaphore Sem : Reference of the Semaphore sem for which the thread is waiting.
*
*   Returns:
*   void.
*   
*/

void removeFromWaitList(MySemaphore semaphore){
	wait_q *q = wait_head;
	wait_q *r = wait_head->next;
	semaphore_q *sem = (semaphore_q *)semaphore;
	if(q->sem_blocked_on == sem)
		wait_head = wait_head->next;
	else {
		while(r!=NULL){
			if(r->sem_blocked_on == sem){
				q->next = r->next;
				break;
			}
			r = r->next;
			q = q->next;
		}
	}
}


/*
*   MySemaphoreDestroy(MySemaphore sem) : Destroys Semaphore sem.
*   
*   Input:
*   MySemaphore Sem : Reference of the Semaphore sem to destroy.
*
*   Returns:
*   0 : On Success
*  -1 : On Failure. Failure occurs when a thread tries to destroy a semaphore on which other threads are waiting.
*/

int MySemaphoreDestroy(MySemaphore sem){
	wait_q *q = wait_head;
	semaphore_q *sem1 = sem_head;
	semaphore_q *sem2;
	if(sem_head != NULL)
		sem2 = sem_head -> next;
	while(q!=NULL){
		if(q->sem_blocked_on == sem)
			return -1;
		q = q->next;
	}
	
	// We need to destroy the semaphore and release the memory
	while(sem_head!=NULL){
		if(sem_head == sem){		
			sem_head = sem_head -> next;
			break;
		}
		else{
			while(sem2!=NULL){
				if(sem2 == sem){
					sem1->next = sem2->next;
					break; 
				}
				sem1 = sem1->next;
				sem2 = sem2->next;		
			}
			break;
		}
	}
	return 0;
}


/******************************** Thread Library Ends*************************/
